#include "Vector.h"
#include <iostream>
using namespace std;

void set_console_color(unsigned int color)
{
	// colors are 0=black 1=blue 2=green and so on to 15=white
	// colorattribute = foreground + background * 16
	// to get red text on yellow use 4 + 14*16 = 228
	// light red on yellow would be 12 + 14*16 = 236
	// a Dev-C++ tested console application by vegaseat 07nov2004

	HANDLE hConsole;

	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, color);
}

//builders
Vector::Vector(int n)
{
	if (n < 2)
	{
		n = 2;
	}
	this->_resizeFactor = n;
	this->_elements = new int[n];
	this->_capacity = n;
	this->_size = 0;
}
Vector::~Vector()
{
	delete this->_elements;
}

//size
void Vector::reserve(int n)
{
	int len = (n / _resizeFactor)*_resizeFactor;
	if (n % _resizeFactor != 0)
	{
		len += _resizeFactor;
	}
	int * tmp  = new int[len];
	int i = 0;
	if (_capacity<=n)
	{	
		for (i = 0; i < _size; i++)
		{
			tmp[i] = _elements[i];
		}
		_elements = new int[len];
	}
	
	_elements = tmp;
	_capacity = len;
	//delete[] tmp;
}
void Vector::assign(int val)
{
	for (int i = 0; i < _size; i++)
	{
		_elements[i] = val;
	}
}
void Vector::resize(int n)
{
	if (_capacity > n)
	{
		int * tmp = new int[_capacity];
		for (int i = 0; i <= n; i++)
		{
			tmp[i] = _elements[i];
		}
		_size = n;
		_elements = tmp;
	}
	else
	{
		reserve(n);
	}
}
void Vector::resize(int n, const int& val)
{
	int len = (n / _resizeFactor)*_resizeFactor;;
	int * tmp = new int[len];
	if (_capacity>n)
	{
		for (int i = 0; i < _size; i++)
		{
			tmp[i] = val;
		}
		this->_elements = tmp;
	}
	else
	{
		int * tmp = new int[_capacity];
		for (int i = 0; i <= n; i++)
		{
			tmp[i] = _elements[i];
		}
		_size = n;
		_elements = tmp;
	}
	
	//delete[] tmp;
}


//setters
void Vector::push_back(const int & val)
{
	if (_size == _capacity)
	{
		reserve(_resizeFactor);
	}
	else
	{
		int tmp = val;
		this->_elements[_size] = tmp;
		_size += 1;
	}
}
int Vector::pop_back()
{
	if (this->_elements[0] == NULL)
	{
		cerr << "error: pop from empty vector";
		system("pasue");
		exit(-9999);
	}
	else
	{
		return this->_elements[this->_size];
	}
}


//getters
int Vector::size() const
{
	int tmp = _size;
	return tmp;
}
int Vector::capacity() const
{
	int tmp = _capacity;
	return tmp;
}
int Vector::resizeFactor() const
{
	int tmp = _resizeFactor;
	return tmp;
}
bool Vector::empty() const
{
	if (this->_elements == NULL)
	{
		return false;
	}
	else
	{
		return true;
	}
}
void Vector::print()
{
	set_console_color(RED);
	cout << "Vector info:"<<endl;
	set_console_color(YELLOW);
	cout << "Capacity is:" <<this->_capacity<< endl;
	cout << "size is:" <<this->_size<< endl;
	set_console_color(GREEN);
	cout << "data is:" << endl;
	for (int i = 0; i < this->_size; i++)
	{
		cout << i + 1 << ". " << this->_elements[i] << endl;
	}
	set_console_color(WHITE);
}

//opertors
int& Vector::operator[](int n) const
{
	if (n>_size)
	{
		cerr<< "error: cant reach this number!";
		return this->_elements[0];
	}
	return this->_elements[n];
}
Vector::Vector(const Vector& other)
{
	this->_size = other.size();
	this->_capacity = other.capacity();
	this->_resizeFactor = other.resizeFactor();
	this->_elements = new int[_capacity];
	for (int i = 0; i < other.size(); i++)
	{
		this->_elements[i] = other._elements[i];
	}
}
Vector & Vector::operator=(const Vector & other)
{
	this->_capacity = other.capacity();
	this->_size = other.size();
	this->_resizeFactor= other.resizeFactor();
	this->_elements = new int[_capacity];
	for (int i = 0; i < other.size(); i++)
	{
		this->_elements[i] = other._elements[i];
	}
	return *this;
}
